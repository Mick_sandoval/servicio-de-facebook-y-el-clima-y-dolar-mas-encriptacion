
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashSet;

public class Conversion_dolar {
     public class objeto {
        public String clima;
        public String temperatura;
        public String humedad;
        public String conversion;
    }
     objeto prueba = new objeto();
    private HashSet<String> links;

    public Conversion_dolar() {
        links = new HashSet<String>();
    }

    public void getPageLinks(String URL) {
        //4. Check if you have already crawled the URLs 
        //(we are intentionally not checking for duplicate content in this example)
        if (!links.contains(URL)) {
            try {
                //4. (i) If not add it to the index
                if (links.add(URL)) {
                    System.out.print("Página Consultada: ");
                    System.out.println(URL);
                    System.out.println("");
                    System.out.println("");
                }
                /*utilizamos la clase calendar para obtener la fecha*/
                Calendar tiempo = Calendar.getInstance();

                //System.out.println(tiempo.getTime());

                //2. Fetch the HTML code
                Document document = Jsoup.connect(URL).get();
                prueba.conversion = document.getElementById("last_last").html();
                //System.out.print("hoy " + tiempo.getTime());
                //System.out.print(" el tipo de cambio promedio del dólar en México es de  ");
                //System.out.println(prueba.conversion );
                /*for (int i = 1; i < 10; i++) {
                     System.out.print(temperatura.charAt(i));
                }*/
                
            } catch (IOException e) {
                System.err.println("For '" + URL + "': " + e.getMessage());
            }
        }
    }

    public static void main(String[] args) {
        //1. Pick a URL from the frontier
        String pagina = "https://mx.investing.com/currencies/usd-mxn-converter";
        new Conversion_dolar().getPageLinks(pagina);
    }

}
