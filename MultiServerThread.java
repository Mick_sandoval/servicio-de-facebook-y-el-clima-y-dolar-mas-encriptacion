import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.types.FacebookType;
import com.restfb.types.User;
import java.io.*;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.DatatypeConverter;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MultiServerThread extends Thread {
   private Socket socket = null;

   public MultiServerThread(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      ServerMultiClient.NoClients++;
   }

   public void run() {
    char nuevacadena[];
    BasicWebCrawler clima = new BasicWebCrawler();
    Conversion_dolar dolar = new Conversion_dolar();
      try {
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn;
		
	    while((lineIn = entrada.readLine()) != null){
                nuevacadena = lineIn.toCharArray();
                int num;
                num = lineIn.length();
            System.out.println("Received: "+lineIn);
            escritor.flush();
            if(lineIn.equals("FIN")){
               ServerMultiClient.NoClients--;
                break;
            }else if(lineIn.equals("#clientes#")){//Cantidad de clientes conectados
               escritor.println("Clientes conectados: "+ServerMultiClient.NoClients);
               escritor.flush();
            }else if(lineIn.charAt(0)=='#' && lineIn.charAt(1)=='1' && lineIn.charAt(2)=='|'){//Encripta una cadena
                //CADENA EJEMPLO PARA USAR EL ENCRIPTADO: #1|Hola
                String subLine = lineIn.substring(3, lineIn.length());
                String cadenaEncriptada = encriptar(subLine);

                escritor.println("CADENA ENCRIPTADA: "+cadenaEncriptada);
                escritor.flush();  
            }else if(lineIn.charAt(0)=='#' && lineIn.charAt(1)=='2' && lineIn.charAt(2)=='|'){//Desencripta una cadena
                //CADENA EJEMPLO PARA USAR EL DESENCRIPTADO: #2|SG9sYQ==
                String subLine = lineIn.substring(3, lineIn.length());
                String cadenaDesencriptada = desencriptar(subLine);

                escritor.println("CADENA DESENCRIPTADA: "+cadenaDesencriptada);
                escritor.flush();
            }else if(lineIn.charAt(0)=='#'){
                System.out.println("Entramos al servicio");
                if(lineIn.charAt(lineIn.length()-1)=='&'){
                    System.out.println("Entramos al if para checar sintaxis");
                    if(lineIn.charAt(1)=='6' && lineIn.charAt(2)=='2' && lineIn.charAt(3)=='2'&& lineIn.charAt(4)=='|'){
                        System.out.println("Entramos al if del codigo");
                        String subLine = lineIn.substring(5, lineIn.length()-1);
                        escritor.println(" "+ subLine);
                        escritor.flush();
                         String  n,sn ="";
        int x=0;
        for (int i=0; i<subLine.length(); i++){
        x=subLine.charAt(i);
        n=Integer.toBinaryString(x);
        sn= sn+n;
        System.out.println(subLine.charAt(i));
        System.out.println(x);
        System.out.println(n+"\n");
        }
        escritor.println(" "+sn);
        escritor.flush();
                        
        }
                    
                    
                    if (lineIn.charAt(1)=='6' && lineIn.charAt(2)=='1' && lineIn.charAt(3)=='9'&& lineIn.charAt(4)=='|') {
                        
                        clima.getPageLinks("https://www.meteored.mx/clima_Nueva+York-America+Norte-Estados+Unidos-Livingston-KNYC-1-11290.html");
                        int num2 = clima.prueba.temperatura.length();
                        //escritor.println(clima.prueba.clima);
                            escritor.print("el valor de la tempratura es:  ");
                        for (int i = 56; i < (num2 - 7); i++) {
                            escritor.print(clima.prueba.temperatura.charAt(i));
                        }
                            escritor.println("C");
                    }
                    
                    if (lineIn.charAt(1)=='3' && lineIn.charAt(2)=='0' && lineIn.charAt(3)=='1'&& lineIn.charAt(4)=='|') {
                        escritor.print("CONVERSIÓN PESOS MEXICANOS A DOLAR");
                        dolar.getPageLinks("https://mx.investing.com/currencies/usd-mxn-converter");
                        escritor.println();
                        escritor.print(" el tipo de cambio promedio del dólar en México es de " + dolar.prueba.conversion);
                        escritor.println();
                    }
                    
                    if (lineIn.charAt(1)=='5' && lineIn.charAt(2)=='7' && lineIn.charAt(3)=='8'&& lineIn.charAt(4)=='|') {
                        String r = lineIn.substring(5, lineIn.length()-1);
                        HttpResponse re;
                        String accessToken = "EAAekdO1RMFIBANNuqXv6ZAArAmFNozMJ5BZC8U4F6XpTquErwoZA5UyRtzolWskrNmCVZA5Lb5fnd0UbVk3mWDE3DmUZBThZB1z5q3ZBc4iuN7KEdq3tIBHVSKU6WwpIdNI1Js84NFpd86uVpQ44ZChT7IRY3LtxvW5z6gNU3aZCJA64D3OQo31ZCW";
                        re = Request.Post("https://graph.facebook.com/111586106909285/feed/").bodyForm(Form.form().add("message", r).add("access_token", accessToken).build()).execute().returnResponse();
                        escritor.println("Publicado");
                    }
                    
                    else{
                        escritor.println("ERROR! ->codigo incorrecto<-");
                        escritor.flush();
                    }
                }else{
                    escritor.println("ERROR! ->sintaxis incorrecta<-");
                    escritor.flush();
                }
            }else{
                escritor.println("Echo... "+lineIn);
                escritor.flush();
            }
         } 
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(IOException e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      } catch (Exception ex) {
           Logger.getLogger(MultiServerThread.class.getName()).log(Level.SEVERE, null, ex);
       }
   }
   
    public static String toHexadecimal(String text) throws UnsupportedEncodingException{
        byte[] myBytes = text.getBytes("UTF-8");
        return DatatypeConverter.printHexBinary(myBytes);
    }
    
    public static String convertArrayToString(String[] strArray) {
        String joinedString = String.join("|", strArray);
        return joinedString;
    }
    
    private static String encriptar(String s) throws UnsupportedEncodingException{
        return Base64.getEncoder().encodeToString(s.getBytes("utf-8"));
    }
    
    private static String desencriptar(String s) throws UnsupportedEncodingException{
        byte[] decode = Base64.getDecoder().decode(s.getBytes());
        return new String(decode, "utf-8");
    }
   
} 

